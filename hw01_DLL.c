// James McConkey
// Elizabeth Lee
// CSE310-SU14: Assignment 1
// Create nodes, delete nodes, sort nodes of doubly linked lists

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "graphvisualize.h"

//Global variable for the head
struct drecord *head;

// FORWARD DECLARATIONS
int insert (int value);
int delete (int value);
struct drecord *swap(struct drecord* A, struct drecord* B);
struct drecord *sort(struct drecord *top);
void print(struct drecord* top);
void free_list(struct drecord* top);
int main(int argc, char *argv[]);

//Insert at the beginning of the list
int insert(int value){
	struct drecord *np = (struct drecord *) malloc(sizeof(struct drecord));
	//If head is null - first item in the list
	if(head == NULL){
		np->value = value;
        np->previous = NULL;
        np->next = NULL;
        head = np;
	}
	//One or more items currently in the list
	else{
        np->value = value;
        np->next = head;
		if (head->previous == NULL)
			np->previous = head;
		else
        	np->previous = head->previous;
        head->previous = np;
        head = np;
    }
}

int delete(int value){
	//struct drecord *np = (struct drecord *) malloc(sizeof(struct drecord));
    //Empty list
    
    if(head == NULL){
        printf("The list is empty so you cannot delete any data");
    }
    //Deleting the only item
    else if(head->next == NULL && head->previous == NULL && head->value == value){
        free(head);
        head = NULL;
    }
    //Deleting the first item if list only has two nodes
    else if(head->next->next == NULL && head->value == value){
        head = head->next;
        free(head->previous);
        head->previous = NULL;
    }
    //Deleting the last item if list only has two nodes
    else if(head->next->next == NULL && head->next->value == value){
        free(head->next);
        head->previous = NULL;
        head->next = NULL;
    }
    //Deleting the first item if the list has more than 2 nodes
    else if(head->value == value){
        struct drecord *temp = head;
        head->next->previous = head->previous;
        head = head->next;
        free(temp);
    }
    //Deleting item in the middle of the list
    else{ //if(head->next != NULL && head->previous != NULL){
        struct drecord *np = head;
        //Want to search the list
        while(np->next != NULL){
            //free
            if(np->value == value){
                //Currently on node to be freed
                np->next->previous = np->previous;
                np->previous->next = np->next;
                free(np);
                break;
                break;
            }
            
            np = np->next;
        }
        //Deleting the tail
        if(np->next == NULL && np->value == value){
            //Currently on tail
            head->previous = np->previous;
            np->previous->next = NULL;
            free(np);
        }
    }
}


// switch the values
struct drecord* swap(struct drecord* A, struct drecord* B){
	int tempX = A->value;
	A->value = B->value;
	B->value = tempX;
	return A;
}

struct drecord *sort(struct drecord *top) {
	if (top == NULL) {
		return NULL;
	}
	//check first two items, then call sort on the rest of the list
	// bring larger values towards tail
	if (top->next != NULL && top->value > top->next->value) {
		top = swap(top, top->next);
	}
	top->next = sort(top->next);
	
	//recheck first two items in case the first changed
	// then call sort on the rest of the list.
	// bring smaller values towards head
	if (top->next != NULL && top->value > top->next->value) {
		top = swap(top, top->next);
		top->next = sort(top->next);
	}
	return top;
}

// debugging print function
void print(struct drecord* top) {
	while (top != NULL) {
		printf("%d  ", top->value);
		top = top->next;
	}
	printf("\n");
}

void free_list(struct drecord* top) {
	if (top != NULL) {
		free_list(top->next);
		free(top);
	}
}

//main
int main(int argc, char *argv[])
{
    //Create buffers to hold the string and ints read in from the file
    char stringbuffer[32];
    int intbuffer;
	
	// calculate runtime
	clock_t start, end;
	double secs;
	
	start = clock();
	
	FILE *dotfile;
	int count = 0;
	char str[80];
    
    //Set the file to the second argument in the command line
    FILE *commandfile = fopen(argv[1], "rb");
    
    while (fscanf(commandfile, "%s %d", stringbuffer, &intbuffer) != EOF)
    {   
        //compare different char arrays and call the appropriate function
        if (strcmp(stringbuffer, "insert") == 0) {
            insert(intbuffer);
        }
        else if(strcmp(stringbuffer, "delete") == 0){
            delete(intbuffer);
        }
        else if(strcmp(stringbuffer, "sort") == 0){
            head = sort(head);  //I assume that your sort needs
        }
        else{
            printf("Invalid command present in command file");
        }
		
		// make DOT file
		count = count+1;
		sprintf(str, "dcommand-%04d.dot", count);
		dotfile = fopen(str, "wb");
		fprintf(dotfile, "%s", visualize_dlist(head));
		fclose(dotfile);
    }
	
	
	end = clock();
	secs = (end - start)*1000.0/ CLOCKS_PER_SEC;
	printf("runtime is %f milliseconds\n", secs);
	
	// free linked list from memory
	free_list(head);
    
    return 0;
}