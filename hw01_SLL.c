// Elizabeth Lee
// James McConkey
// CSE310-SU14: Assignment 1
// Create nodes, delete nodes, sort nodes of singly linked lists

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "graphvisualize.h"

// GLOBAL VARIABLES
struct record *head = NULL;

// FORWARD DECLARATIONS
int insert(int key);
int delete(int key);
struct record *swap(struct record* A, struct record* B);
struct record *sort(struct record *top);
void print(struct record* top);
void free_list(struct record* top);
int main(int argc, char *argv[]);


// insert at the beginning
int insert(int key) {
    //Allocate memory for a new node
	struct record *newnode = (struct record *) malloc(sizeof(struct record));
    
    //Set the value of the new node
    newnode->value = key;
    
    //If head is NULL then this is the first item in the list
	if (head == NULL){
        head = newnode;
		head->next = NULL;
		return 0;
	}
    //Else there are multiple items.  Insert at the head.
	else {
        newnode->next = head;
        head = newnode;
        return 0;
	}
}

// delete by finding the key
// return 0 if delete successful
// return -1 if not found
int delete(int key) {
	struct record* temp = (struct record *) malloc(sizeof(struct record));
	temp = head;
	if (head == NULL){
		printf("No linked list found");
        free(temp);
		return -1;
	}
	else {
		if (temp->value == key){
			head = temp->next;
			free(temp);
			return 0;
		}
		while (temp->next != NULL) {
			if (temp->next->value == key) {
				temp->next = temp->next->next;
				return 0;
				break;
				break;
			}
			temp = temp->next;
		}
		if (temp->next == NULL) {
			printf("\n%d not found in linked list", key);
			return -1;
		}
	}
}

struct record* swap(struct record* A, struct record* B){
	A->next = B->next;
	B->next = A;
	return B;
}

struct record *sort(struct record *top) {
	if (top == NULL) {
		return NULL;
	}
	//check first two items, then call sort on the rest of the list
	// bring larger values towards tail
	if (top->next != NULL && top->value > top->next->value) {
		top = swap(top, top->next);
	}
	top->next = sort(top->next);
	
	//recheck first two items in case the first changed
	// then call sort on the rest of the list.
	// bring smaller values towards head
	if (top->next != NULL && top->value > top->next->value) {
		top = swap(top, top->next);
		top->next = sort(top->next);
	}
	return top;
}

// debugging print function
void print(struct record* top) {
	while (top != NULL) {
		printf("%d\t", top->value);
		top = top->next;
	}
	printf("\n");
}

// memory management
void free_list(struct record* top) {
	if (top != NULL) {
		free_list(top->next);
		free(top);
	}
}

//main
int main(int argc, char *argv[])
{
    //Create buffers to hold the string and ints read in from the file
    char stringbuffer[32];
    int intbuffer;
	
	// clock variables
	clock_t start, end;
	double secs;
	
	start = clock();
	
	FILE *dotfile;
	int count = 0;
	char str[80];
	    
    //Set the file to the second argument in the command line
    FILE *commandfile = fopen(argv[1], "rb");
    
    while (fscanf(commandfile, "%s %d", stringbuffer, &intbuffer) != EOF)
    {   
        //compare different char arrays and call the appropriate function
        if (strcmp(stringbuffer, "insert") == 0) {
            insert(intbuffer);
        }
        else if(strcmp(stringbuffer, "delete") == 0){
            delete(intbuffer);
        }
        else if(strcmp(stringbuffer, "sort") == 0){
            head = sort(head);
        }
        else{
            printf("Invalid command present in command file");
        }
		
		// make DOT file
		count = count+1;
		sprintf(str, "scommand-%04d.dot", count);
		dotfile = fopen(str, "wb");
		fprintf(dotfile, "%s", visualize_list(head));
		fclose(dotfile);
    }
    //close the file
    fclose(commandfile);
	
	// get the run time
	end = clock();
	secs = (end - start)*1000.0/ CLOCKS_PER_SEC;
	printf("runtime is %f milliseconds\n", secs);
	
	free_list(head);
    
    return 0;
}