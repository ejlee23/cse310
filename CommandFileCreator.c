#include <string.h>
#include <stdio.h>

#define COMMAND_FILE "command.txt"

int main()
{
FILE *commandfile;
//string to hold "insert"
char insert[] = "insert";
char sort[] = "sort";
int i, n=0;
srand(time(NULL)); //Seed the random number using time


commandfile = fopen(COMMAND_FILE, "wb");

for(i=1; i<250; i++)
{
//Create a random number between 0-9
n = rand() % 1000;


       fprintf(commandfile, "%s", insert);
       fprintf(commandfile, " ");
       fprintf(commandfile, "%d", n);
fprintf(commandfile, "\n");
}
//Write the sort command at the end of the file
   fprintf(commandfile, "%s", sort);
   fclose(commandfile);
   return 0;
}