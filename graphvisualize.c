/********************************************************************
 ********************************************************************
 * Graphvisualize: a small library for data structure visualization *
 * Georgios Varsamopoulos georgios.varsamopoulos@asu.edu            *
 * CSE310 SUMMER 2012 - Data Structures and Algorithms              *
 * Version 1.5                                                      *
 * v1.0 -> v1.1: Maintainance of pointers array to prevent cycling  *
 *               Commenting of the code                             *
 * v1.1 -> v1.2: Addition of parent pointer to tree node struct     *
 *               Visualization of parent pointers                   *
 *               Commenting of the code                             *
 * v1.2 -> v1.3: a. Freeing memory allocated for strings            *
 *               b. Fixed a bug where a b%p node would be declared  *
 *                  twice                                           *
 *               c. Putting the head->previous edge to the end of   *
 *                  the DOT output to keep nodes aligned            *
 *               d. Compiler compatibility fixes (Student-requested)*
 * v1.3 -> v1.4: Initializing maxknownpointer to -1 with every new  *
 *               call of visualize_tree, visualize_list and         *
 *               visualize_sublist                                  * 
 * v1.4 -> v1.5: Introduction of enum color and height for Red-Black*
 *               and AVL trees. Introduction of visualize_rbtree()  *
 ********************************************************************
 ********************************************************************
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "graphvisualize.h"

#define DOTSTRLEN 20480

char *visualize_subtree(struct node *);
char *visualize_rbsubtree(struct node *);
char *visualize_sublist(struct record *);
char *visualize_subdlist(struct drecord *);

int checkpointer(void *);
int addpointer(void *);
void *knownpointers[250];
int maxknownpointer=-1;

char *visualize_tree(struct node *root)
{
	char *dotstring, *auxstring;
	dotstring = (char *) malloc(DOTSTRLEN*sizeof(char));
	memset(dotstring,0,DOTSTRLEN);

	maxknownpointer=-1;

	sprintf(dotstring,"digraph G {\n");
	sprintf(dotstring,"%s\tnode [fillcolor=none,shape=record];\n",dotstring);
	sprintf(dotstring,"%s\tedge [tailport=\"S\"];\n",dotstring);
	sprintf(dotstring,"%s\n",dotstring);

	
	if (root==NULL) /* if the supplied pointer is null then drawn a grounding */
	{
		sprintf(dotstring,"%sa [style=invisible];\n",dotstring);
		sprintf(dotstring,"%sb [style=invisible];\n",dotstring);
		sprintf(dotstring,"%sa -> b [arrowhead=teetee,arrowtail=dot,dir=both];\n",dotstring);
	}
	else
	{
		addpointer((void*) root); /* add the pointer to the known pointers array */

		/* add the graph node. Naming convention is n%p where %p is the pointer */
		if (root->color == RED)
			sprintf(dotstring,"%s\tn%p [color=red,label=\"{<parent> | <value> k:%d h:%d| {<left> |<right> }}\"];\n", dotstring, root, root->value, root->height);
		else
			sprintf(dotstring,"%s\tn%p [color=black,label=\"{<parent> | <value> k:%d h:%d| {<left> |<right> }}\"];\n", dotstring, root, root->value, root->height);
			
		if (root->left!=NULL)  /* edge to the left child */
			sprintf(dotstring,"%s\tn%p:left -> n%p;\n", dotstring, root, root->left);

		if (root->left!=NULL && root->right==NULL)  /* must draw invisible right child */
		{
			sprintf(dotstring,"%s\tn%p:right -> r%p [style=invisible,arrowhead=none];\n",dotstring,root,root);
			sprintf(dotstring,"%s\tr%p [label=\" \\ \",style=invisible];\n", dotstring, root);
		}

		if (root->right!=NULL) /* edge to the right child */
			sprintf(dotstring,"%s\tn%p:right -> n%p;\n", dotstring, root, root->right);

		if (root->right!=NULL && root->left==NULL)  /* must draw invisible left child */
		{
			sprintf(dotstring,"%s\tn%p:left -> l%p [style=invisible,arrowhead=none];\n", dotstring, root, root);
			sprintf(dotstring,"%s\tl%p [label=\" \\ \",style=invisible];\n", dotstring, root);
		}

		if (root->left!=NULL)  /* "parse" the left subtree */
		{
			auxstring = visualize_subtree(root->left);
			strcat(dotstring, auxstring);
			free(auxstring);
		}

		if (root->right!=NULL) /* "parse" the right subtree */
		{
			auxstring = visualize_subtree(root->right);
			strcat(dotstring, auxstring);
			free(auxstring);
		}
	}

	sprintf(dotstring,"%s}\n",dotstring);
	
	return dotstring;
}


char *visualize_subtree(struct node *root)
{
	char *dotstring, *auxstring;
	dotstring = (char *) malloc(DOTSTRLEN*sizeof(char));
	memset(dotstring,0,DOTSTRLEN);

	if (checkpointer((void *) root)==0)
		addpointer((void*) root); /* add the pointer to the known pointers array */
	else
		return ""; /* visited this vertex, do not go through this again */

	if (root==NULL) /* if the supplied pointer is null then drawn a grounding */
	{
		sprintf(dotstring,"\t%s\tn%p [style=invisible];\n",
			dotstring, root);
		sprintf(dotstring,"\t%s\tb%p [style=invisible];\n",
			dotstring, root);
		sprintf(dotstring,"\t%s\tn%p -> b%p [arrowhead=teetee,arrowtail=dot,dir=both];\n",
			dotstring, root, root);
	}
	else
	{
		/* add the graph node. Naming convention is n%p where %p is the pointer */
		if (root->color == RED)
			sprintf(dotstring,"%s\tn%p [color=red,label=\"{<parent> | <value> k:%d h:%d| {<left> |<right> }}\"];\n", dotstring, root, root->value, root->height);
		else
			sprintf(dotstring,"%s\tn%p [color=black,label=\"{<parent> | <value> k:%d h:%d| {<left> |<right> }}\"];\n", dotstring, root, root->value, root->height);
			
		if (root->parent!=NULL)
			sprintf(dotstring,"%s\tn%p:parent -> n%p;\n",
				dotstring, root, root->parent);
			
		if (root->left!=NULL) /* edge to the left child */
			sprintf(dotstring,"%s\tn%p:left -> n%p;\n",
				dotstring, root, root->left);

		if (root->left!=NULL && root->right==NULL)  /* must draw invisible right child */
		{
			sprintf(dotstring,"%s\tn%p:right -> r%p [style=invisible,arrowhead=none];\n", dotstring, root, root);
			sprintf(dotstring,"%s\tr%p [label=\" \\ \",style=invisible];\n", dotstring, root);
		}


		if (root->right!=NULL) /* edge to the right child */
			sprintf(dotstring,"%s\tn%p:right -> n%p;\n",
				dotstring, root, root->right);

		if (root->right!=NULL && root->left==NULL)  /* must draw invisible left child */
		{
			sprintf(dotstring,"%s\tn%p:left -> l%p [style=invisible,arrowhead=none];\n", dotstring, root, root);
			sprintf(dotstring,"%s\tl%p [label=\" \",style=invisible];\n", dotstring, root);
		}


		if (root->left!=NULL) /* "parse" the left subtree */
		{
			auxstring = visualize_subtree(root->left);
			strcat(dotstring, auxstring);
			free(auxstring);
		}

		if (root->right!=NULL) /* "parse" the right subtree */
		{
			auxstring = visualize_subtree(root->right);
			strcat(dotstring, auxstring);
			free(auxstring);
		}
	}
	
	return dotstring;
}


char *visualize_list(struct record *head)
{
	char *dotstring;
	struct record *cur;
	dotstring = (char *) malloc(DOTSTRLEN*sizeof(char));
	memset(dotstring,0,DOTSTRLEN);

	maxknownpointer=-1;

	sprintf(dotstring,"digraph G {\n");
	strcat(dotstring,"\tgraph [rankdir=\"LR\"];\n");
	strcat(dotstring,"\tnode [shape=record];\n");
	strcat(dotstring,"\n");


	strcat(dotstring,"\ta [style=invisible];\n");
	if(head==NULL) /* the supplied pointer is null, then drawn a grounding */
	{
		strcat(dotstring,
			"\tb [style=invisible];\n");
		strcat(dotstring,
			"\ta -> b [arrowhead=teetee,arrowtail=box,dir=both];\n");
	}
	else
		sprintf(dotstring,
			"%s\ta -> n%p [arrowtail=box,dir=both];\n",
			dotstring, head);

	for(cur=head; cur!=NULL; cur=cur->next)
	{
		/* See if we've been to this pointer before */
		if (checkpointer((void *) cur)==0)
			addpointer((void*) cur);
		else
			break;

		/* add the graph node. Naming convention is n%p where %p is the pointer */
		sprintf(dotstring,
			"%s\tn%p [label=\"{<value> %d | <next>  }\"];\n",
			dotstring, cur, cur->value);
			
		if (cur->next != NULL)
			sprintf(dotstring,"%s\tn%p:next -> n%p;\n",
				dotstring, cur, cur->next);
		else
		{
			/* 
			 * create a grounding for the next.
			 * The b%p node is an auxiliary invisible node to do
                         * the grounding. The b%p node must have the same
                         * record structure as a regular node, otherwise
			 * alignment * using "same rank" will not work.
			 */ 
			sprintf(dotstring,
				"%s\tb%p [label=\"{<value> %d| <next>}\",style=invisible];\n",
				dotstring,cur,cur->value);
			sprintf(dotstring,"%s\t{rank=same; n%p; b%p}\n",
				dotstring,cur,cur);
			sprintf(dotstring,
				"%s\tn%p:next -> b%p:next [arrowhead=none,arrowtail=teetee,dir=both];\n",
				dotstring, cur, cur);
		}
	}
	strcat(dotstring,"}\n");
	
			
	return dotstring;
}

char *visualize_dlist(struct drecord *head)
{
	char *dotstring;
	struct drecord *cur;
	dotstring = (char *) malloc(DOTSTRLEN*sizeof(char));
	memset(dotstring,0,DOTSTRLEN);

	maxknownpointer=-1;

	sprintf(dotstring,"digraph G {\n");
	strcat(dotstring,"\tgraph [rankdir=\"LR\"];\n");
	strcat(dotstring,"\tnode [shape=record];\n");
	strcat(dotstring,"\n");


	strcat(dotstring,"\ta [style=invisible];\n");
	if(head==NULL)
	{
		strcat(dotstring,
			"\tb [style=invisible];\n");
		strcat(dotstring,
			"\ta -> b [arrowhead=teetee,arrowtail=box,dir=both];\n");
	}
	else
	{
		sprintf(dotstring,
			"%s\ta -> n%p [arrowtail=box,dir=both];\n",
			dotstring, head);
	}

	for(cur=head; cur!=NULL; cur=cur->next)
	{
		if (checkpointer((void *) cur)==0)
			addpointer((void*) cur);
		else
			break;
		
		/* add the graph node. Naming convention is n%p where %p is the pointer */
		sprintf(dotstring,
			"%s\tn%p [label=\"{<previous>|<value>%d|<next>}\"];\n",
			dotstring, cur, cur->value);
		

		if (cur->previous != NULL && cur!=head) 
			/* label=" " makes dot draw a separate edge backward. Otherwise
			 * dot draws a double-headed edge
			 */
			sprintf(dotstring,"%s\tn%p:previous -> n%p [label=\"  \",weight=1];\n",
				dotstring, cur, cur->previous);
		else if (cur->previous == NULL && cur!=head)
		{
			/* 
			 * create a grounding for the next.
			 * The b%p node is an auxiliary invisible node to do
                         * the grounding. The b%p node must have the same
                         * record structure as a regular node, otherwise
			 * alignment * using "same rank" will not work.
			 */ 
			sprintf(dotstring,
				"%s\tb%p [label=\"{<previous>|<value> %d|<next>}\",style=invisible];\n",
				dotstring,cur,cur->value);
			sprintf(dotstring,"%s\t{rank=same; n%p; b%p}\n",
				dotstring,cur,cur);
			sprintf(dotstring,
				"%s\tn%p:previous -> b%p:previous [arrowhead=none,arrowtail=teetee,dir=both,weight=1000];\n",
				dotstring, cur, cur);
		}
			
		if (cur->next != NULL)
			sprintf(dotstring,"%s\tn%p:next -> n%p [label=\" \",weight=100];\n",
				dotstring, cur, cur->next);
		else if (cur->previous != NULL)
		{
			/* 
			 * create a grounding for the previous.
			 * The b%p node is an auxiliary invisible node to do
                         * the grounding. The b%p node must have the same
                         * record structure as a regular node, otherwise
			 * alignment * using "same rank" will not work.
			 */ 
			sprintf(dotstring,
				"%s\tb%p [label=\"{<previous>|<value>%d|<next>}\",style=invisible];\n",
				dotstring,cur,cur->value);
			sprintf(dotstring,"%s\t{rank=same; n%p; b%p}\n",
				dotstring,cur,cur);
			sprintf(dotstring,
				"%s\tn%p:next -> b%p:next [arrowhead=none,arrowtail=teetee,dir=both,weight=1000];\n",
				dotstring, cur, cur);
		}
		else
		{
			/* 
			 * the grounding node is already created because of
			 * NULL previous
			 */ 
			sprintf(dotstring,
				"%s\tn%p:next -> b%p:next [arrowhead=none,arrowtail=teetee,dir=both,weight=1000];\n",
				dotstring, cur, cur);
		}
	}

	/* draw the head->previous */
	if (head!=NULL && head->previous != NULL) 
		/* label=" " makes dot draw a separate edge backward. Otherwise
		 * dot draws a double-headed edge
		 */
		sprintf(dotstring,"%s\tn%p:previous -> n%p [label=\"  \",weight=1];\n",
			dotstring, head, head->previous);
	else if (head!=NULL && head->previous == NULL && head->next!=NULL)
	{
		/* 
		 * create a grounding for the next.
		 * The b%p node is an auxiliary invisible node to do
                        * the grounding. The b%p node must have the same
                        * record structure as a regular node, otherwise
		 * alignment * using "same rank" will not work.
		 */ 
		sprintf(dotstring,
		"%s\tb%p [label=\"{<previous>|<value> %d|<next>}\",style=invisible];\n",
				dotstring,head,head->value);
		sprintf(dotstring,"%s\t{rank=same; n%p; b%p}\n",
			dotstring,head,head);
		sprintf(dotstring,
			"%s\tn%p:previous -> b%p:previous [arrowhead=none,arrowtail=teetee,dir=both,weight=1000];\n",
			dotstring, head, head);
	}
	else if (head!=NULL && head->previous == NULL && head->next==NULL)
	{
		/* 
		 * the grounding node is already created because of
		 * NULL previous
		 */ 
		sprintf(dotstring,
			"%s\tn%p:previous -> b%p:previous [arrowhead=none,arrowtail=teetee,dir=both,weight=1000];\n",
			dotstring, head, head);
	
	}
			
	strcat(dotstring,"}\n");
			
	return dotstring;
}

char *visualize_rbtree(struct node *root)
{
	char *dotstring, *auxstring;
	dotstring = (char *) malloc(DOTSTRLEN*sizeof(char));
	memset(dotstring,0,DOTSTRLEN);

	maxknownpointer=-1;

	sprintf(dotstring,"digraph G {\n");
	sprintf(dotstring,"%s\tsplines = false;\n",dotstring);
	sprintf(dotstring,"%s\tnode [fillcolor=none];\n",dotstring);
	sprintf(dotstring,"%s\tedge [arrowhead=none,arrowtail=none];\n",dotstring);
	sprintf(dotstring,"%s\n",dotstring);

	
	if (root==NULL) /* if the supplied pointer is null then drawn a grounding */
	{
		sprintf(dotstring,"%sa [style=invisible];\n",dotstring);
		sprintf(dotstring,"%sb [style=invisible];\n",dotstring);
		sprintf(dotstring,"%sa -> b [arrowhead=teetee,arrowtail=dot,dir=both];\n",dotstring);
	}
	else
	{
		addpointer((void*) root); /* add the pointer to the known pointers array */

		/* add the graph node. Naming convention is n%p where %p is the pointer */
		if (root->color == RED)
			sprintf(dotstring,"%s\tn%p [color=red,label=\"%d\"];\n", dotstring, root, root->value);
		else
			sprintf(dotstring,"%s\tn%p [color=black,label=\"%d\"];\n", dotstring, root, root->value);
			
		if (root->left!=NULL)  /* edge to the left child */
			sprintf(dotstring,"%s\tn%p:sw -> n%p:n;\n", dotstring, root, root->left);
		else//if (root->right!=NULL && root->left==NULL)  /* must draw invisible left child */
		{
			sprintf(dotstring,"%s\tn%p:sw -> l%p:nw [style=invisible,arrowhead=none];\n", dotstring, root, root);
			sprintf(dotstring,"%s\tl%p [label=\" \\ \",style=invisible];\n", dotstring, root);
		}


		if (root->right!=NULL) /* edge to the right child */
			sprintf(dotstring,"%s\tn%p:se -> n%p:n;\n", dotstring, root, root->right);
		else //if (root->left!=NULL && root->right==NULL)  /* must draw invisible right child */
		{
			sprintf(dotstring,"%s\tn%p:se -> r%p:nw [style=invisible,arrowhead=none];\n",dotstring,root,root);
			sprintf(dotstring,"%s\tr%p [label=\" \\ \",style=invisible];\n", dotstring, root);
		}


		if (root->left!=NULL)  /* "parse" the left subtree */
		{
			auxstring = visualize_rbsubtree(root->left);
			strcat(dotstring, auxstring);
			free(auxstring);
		}

		if (root->right!=NULL) /* "parse" the right subtree */
		{
			auxstring = visualize_rbsubtree(root->right);
			strcat(dotstring, auxstring);
			free(auxstring);
		}
	}

	sprintf(dotstring,"%s}\n",dotstring);
	
	return dotstring;
}


char *visualize_rbsubtree(struct node *root)
{
	char *dotstring, *auxstring;
	dotstring = (char *) malloc(DOTSTRLEN*sizeof(char));
	memset(dotstring,0,DOTSTRLEN);

	if (checkpointer((void *) root)==0)
		addpointer((void*) root); /* add the pointer to the known pointers array */
	else
		return ""; /* visited this vertex, do not go through this again */

	if (root==NULL) /* if the supplied pointer is null then drawn a grounding */
	{
		sprintf(dotstring,"\t%s\tn%p [style=invisible];\n",
			dotstring, root);
		sprintf(dotstring,"\t%s\tb%p [style=invisible];\n",
			dotstring, root);
		sprintf(dotstring,"\t%s\tn%p -> b%p [arrowhead=teetee,arrowtail=dot,dir=both];\n",
			dotstring, root, root);
	}
	else
	{
		/* add the graph node. Naming convention is n%p where %p is the pointer */
		if (root->color == RED)
			sprintf(dotstring,"%s\tn%p [color=red,label=\"%d\"];\n", dotstring, root, root->value);
		else
			sprintf(dotstring,"%s\tn%p [color=black,label=\"%d\"];\n", dotstring, root, root->value);
			
		/* we will not draw parents 
		 * if (root->parent!=NULL)
		 *	sprintf(dotstring,"%s\tn%p:parent -> n%p;\n",
		 *		dotstring, root, root->parent);
		 */
			
		if (root->left!=NULL) /* edge to the left child */
			sprintf(dotstring,"%s\tn%p:sw -> n%p:n;\n",
				dotstring, root, root->left);
		else //if (root->right!=NULL && root->left==NULL)  /* must draw invisible left child */
		{
			sprintf(dotstring,"%s\tn%p:sw -> l%p:ne [style=invisible,arrowhead=none];\n", dotstring, root, root);
			sprintf(dotstring,"%s\tl%p [shape=circle,label=\" \",style=invisible];\n", dotstring, root);
		}




		if (root->right!=NULL) /* edge to the right child */
			sprintf(dotstring,"%s\tn%p:se -> n%p:n;\n",
				dotstring, root, root->right);
		else //if (root->left!=NULL && root->right==NULL)  /* must draw invisible right child */
		{
			sprintf(dotstring,"%s\tn%p:se -> r%p:nw [style=invisible,arrowhead=none];\n", dotstring, root, root);
			sprintf(dotstring,"%s\tr%p [shape=circle,label=\" \\ \",style=invisible];\n", dotstring, root);
		}


		if (root->left!=NULL) /* "parse" the left subtree */
		{
			auxstring = visualize_rbsubtree(root->left);
			strcat(dotstring, auxstring);
			free(auxstring);
		}

		if (root->right!=NULL) /* "parse" the right subtree */
		{
			auxstring = visualize_rbsubtree(root->right);
			strcat(dotstring, auxstring);
			free(auxstring);
		}
	}
	
	return dotstring;
}


int checkpointer(void *p)
{
	int i;

	for(i=0; i<=maxknownpointer; i++)
	{
		if (p==knownpointers[i])
			return 1;
	}
	return 0;
}

int addpointer(void *p)
{
	maxknownpointer++;
	knownpointers[maxknownpointer] = p;
	
	return 0;
}