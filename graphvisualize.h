/********************************************************************
 ********************************************************************
 * Graphvisualize: a small library for data structure visualization *
 * Georgios Varsamopoulos georgios.varsamopoulos@asu.edu            *
 * CSE310 SUMMER 2012 - Data Structures and Algorithms              *
 * Version 1.5                                                      *
 * v1.0 -> v1.1: Maintainance of pointers array to prevent cycling  *
 *               Commenting of the code                             *
 * v1.1 -> v1.2: Addition of parent pointer to tree node struct     *
 *               Visualization of parent pointers                   *
 *               Commenting of the code                             *
 * v1.2 -> v1.3: a. Freeing memory allocated for strings            *
 *               b. Fixed a bug where a b%p node would be declared  *
 *                  twice                                           *
 *               c. Putting the head->previous edge to the end of   *
 *                  the DOT output to keep nodes aligned            *
 *               d. Compiler compatibility fixes (Student-requested)*
 * v1.3 -> v1.4: Initializing maxknownpointer to -1 with every new  *
 *               call of visualize_tree, visualize_list and         *
 *               visualize_sublist                                  * 
 * v1.4 -> v1.5: Introduction of enum color and height for Red-Black*
 *               and AVL trees. Introduction of visualize_rbtree()  *
 ********************************************************************
 ********************************************************************
 */

enum color {BLACK, RED};

struct node { /* use this struct for trees */
	int value;
	enum color color;
	int height;
	struct node *parent;
	struct node *left;
	struct node *right;
};

struct record { /* use this struct for single-linked lists */
	int value;
	struct record *next;
};

struct drecord { /* use this struct for double-linked lists */
	int value;
	struct drecord *previous;
	struct drecord *next;
};

extern char *visualize_tree(struct node *);
extern char *visualize_rbtree(struct node *);
extern char *visualize_list(struct record *);
extern char *visualize_dlist(struct drecord *);