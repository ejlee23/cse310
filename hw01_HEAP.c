#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "graphvisualize.h"

//Pointers for the root and for the last item inserted
struct node *root, *last;
int count = 0;


void insert(int value);
void delete(int key);
void heapify(struct node* n);
void sort(struct node *rp);
void print(struct node *top);
void swap(struct node* A, struct node* B);
//void free_list(struct node *top);
struct node *search(struct node* current, int key, struct node* found);
struct node *prevlast(struct node *np);
int main(int argc, char *argv[]);


void insert(int value){
   //Create a node with the value passed in, set pointers to null initially
   struct node *np = (struct node *) malloc(sizeof(struct node));
   //Temp is going to be used for traversal
   struct node *temp;
   np->value = value;
   np->parent = NULL;
   np->left = NULL;
   np->right = NULL;
   
	//Case 1, the root is NULL, in which case we need to set the root's value
   if(root == NULL){
       root = np;
   }
	//Case 2, there are still spots left at this level
   else if((int)floor(log(count)/log(2)) == (int)floor(log(count+1)/log(2))){
       //If a left child, there is a spot for a right
       if(last == last->parent->left){
           last->parent->right = np;
           //Set the right node's parent
           last->parent->right->parent = last->parent;
           //Set the "last" node to the most recently added node.
           last = last->parent->right;
       }
       else{
           temp = last;
           //traverse up while the parent is to the right
           while(temp->parent->right == temp){
               temp = temp->parent;
           }
           temp = temp->parent;
           temp = temp->right;
           //Find the leftmost leaf on the right subtree
           while(temp->left != NULL){
               temp = temp->left;
           }
           temp->left = np;
           temp->left->parent = temp;
           last = temp->left;

       }
   }
//Case 3, there are no spots left on the level
   else{
       temp = root;
       //find the leftmost leaf
       while (temp->left != NULL) {
           temp = temp->left;
       }
       temp->left = np;
       temp->left->parent = temp;
       temp = temp->left;
	   last = temp;
   }
   count = count + 1;
   heapify(root);
}

// delete will switch temp with last
// find the previous last
// delete last and reset last
// heapify to maintain heap property
void delete(int key){
	struct node* temp = NULL;
	temp = search(root, key, temp);
	if (temp != NULL){
		printf("\nDelete Node\n");
		// make leaf to delete the last value 
		swap(temp, last);
		// delete last and set temp to previous last and 
		temp = prevlast(last);
		// ensure that the parent doesn't point to last
		if (last->parent->right->value == last->value){
			printf("\nset parent right to null");
			last->parent->right = NULL;
		}
		else{
			printf("\nset parent left to null");			
			last->parent->left = NULL;
		}
		free(last);
		last = temp;
	}
	else {
		printf("Node not found\n");
	}
}


// assume passing in a root and then work with it 
// check to see if the node has no children, if so it is a leaf so stop 
// otherwise check to see if the children are greater 
void heapify(struct node* n) {
	// reached a leaf
	if (n -> left == NULL && n->right == NULL){} 
	// left is greater than current
	else if ((n->left->value > n->value && n->right==NULL) 
	|| (n->left->value > n->value && n->left->value > n->right->value)) { 
		swap(n->left, n);
		heapify(n->left); 
		if (n->parent != NULL && n->value > n->parent->value) {
			heapify(n->parent); 
		} 
	} 
	// right is greater than current
	else if (n->right != NULL && n->right->value > n->value && n->right->value > n->left->value){
		 swap(n->right, n); 
		 heapify(n->right); 
		 if (n->parent != NULL && n->value > n->parent->value) { 
			 heapify(n->parent);
		 }
	 }
	 else{
		 heapify(n->left);
		 if (n->right != NULL)
			 heapify(n->right);
	 }
}

//Sort function-------------------------------
void sort(struct node *rp){
	printf("ENTERED SORT_______________________________________");
	//Create an array to hold the sorted list, should be of size count-1
	int sortedarray[count-1];
	int i;
	struct node *temp;
	
	temp = (struct node *) malloc(sizeof(struct node));
	temp = NULL;
	
	// visualize the heaps made
	FILE *dotfile;
    char str[80];

	//For loop to traverse the array
	for(i=count-1; i>=0 ; i=i-1) {
		printf("\nSORT %d, root value is %d ", i, root->value);
		//The root will be the biggest value
		sortedarray[i] = root->value;
		//Put the largest value in the last node
		swap(root, last);
		//Set the temp node to the previous last
		temp = prevlast(last);
		//make sure the parent points to NULL
		if (temp->parent->right->value == last->value){
			printf("\nset parent right to null");
			last->parent->right = NULL;
		}
		else{
			printf("\nset parent left to null");			
			last->parent->left = NULL;
		}
		//free last
		free(last);
		count = count-1;
		
		//reset last to temp
		last = temp;
		//Heapify the root to maintain the heap property
		heapify(root);
		sprintf(str, "heapsort-%03d.dot", i);
		dotfile = fopen(str, "wb");
		fprintf(dotfile, "%s", visualize_tree(root));
		fclose(dotfile);
	}
}

// switch the values
void swap(struct node* A, struct node* B){
	int tempX = A->value;
	A->value = B->value;
	B->value = tempX;
}

struct node *search(struct node* current, int key, struct node* found) {
	printf("\nstart searching! current value: %d\n", current->value);
    if (found != NULL) {
 	   printf("success! found is %d\n", found->value);
	   return found;
   }
	else{
	if (current->value == key){
		printf("%d found in tree\n", key);
		found = (struct node *) malloc(sizeof(struct node));
		found = current;
		return found;
		}
	else if (current->left == NULL && current->right == NULL) {
		printf("I'm a leaf!\n");
		return found;
	}
	else {
		printf("searching left\n");
		found = search(current->left, key, found);
		if(current->right != NULL && found == NULL) {
			printf("searching right\n");
			found = search(current->right, key, found);			
		}
		return found;
	}
}
}

//Returns a pointer to the previous "last" node
struct node *prevlast(struct node *np){
	struct node *temp;
	printf("\nlog(count) = %d and log(count-1) = %d", (int)(floor(log(count)/log(2))), (int)(floor(log(count-1)/log(2))));
	printf("\ncurrent->parent is %d", np->parent->value);
	if(np->parent == NULL){
		printf("\nLast is root");
	}
	//If the log base 2 of count is not equal to log base 2 of count-1 then the previous last node was
	//on the previous level.
	else if((int)(floor(log(count)/log(2))) != (int)(floor(log(count-1)/log(2)))){
		printf("\nprevlast CASE 1");
		//Set temp to root
		temp = root;
		//Traverse down to the furthest right node
		while(temp->right != NULL){
			temp = temp->right;
		}
		return temp;
	}
	else{
		//Right child
		printf("\nCase2");
		if(np->parent->right != NULL && np == np->parent->right){
		printf("\nprevlast CASE 2.1");
		return np->parent->left;
		}
		//Left child in the middle of a level
		else{
			printf("\nprevlast CASE 2.2");
			temp = np;
			//traverse up to the right all the way
			while(temp == temp->parent->left){
				temp = temp->parent;
			}
			//Go one node up to the left and then one node down to the left
			temp = temp->parent->left;
			//Now traverse all the way down to the right again
			while(temp->right != NULL){
				temp = temp->right;
			}

			return temp;
		}
	}
}

//main
int main(int argc, char *argv[])
{
   //Create buffers to hold the string and ints read in from the file
   char stringbuffer[32];
   int intbuffer;
   FILE *dotfile;
   char str[80];
   
   //temp to test search
   struct node *temp = NULL;
   int find;
   
   //Set the file to the second argument in the command line
   FILE *commandfile = fopen(argv[1], "rb");
   
   while (fscanf(commandfile, "%s %d", stringbuffer, &intbuffer) != EOF)
   {
       //compare different char arrays and call the appropriate function
       if (strcmp(stringbuffer, "insert") == 0) {
           insert(intbuffer);
       }
/*        else if(strcmp(stringbuffer, "delete") == 0){
           delete(intbuffer);
       }*/
       else if(strcmp(stringbuffer, "sort") == 0){
		   sort(root);
       }
	   else{
           printf("Invalid command present in command file\n");
       }
	   
	sprintf(str, "heap-%04d.dot", count);
	dotfile = fopen(str, "wb");
	fprintf(dotfile, "%s", visualize_tree(root));
	fclose(dotfile);
   }
   
   //test search
   find = 736;
   temp = search(root, find, temp);
   if (temp != NULL)
	   printf("success! %d found\n", temp->value);
   else printf("search failed! %d not found\n", find);
   return 0;
}