#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define STR_LEN 32

typedef enum{false, true} bool;

//Structures
struct node{
	int index;
	float probability;
	bool visited;
	char name[STR_LEN];
};

//Global Variables
float best_time;
float **adjacency;
struct node *rooms;
int num_rooms;
char **best_path;
char **path;


// FORWARD DECLARATIONS
int find_index(char *find);
void floyd();
void find_best_path(int location, float distance, float probability, float value, int count);
int main(int argc, char *argv[]);


int find_index(char *find){
	int result = -1;
	for (int i = 0; i < num_rooms; i++){
		if (strcasecmp(find, rooms[i].name) == 0){
			result = i;
			break;
		}
	}
	return result;
}

// runs floyd on the adjacency matrix
void floyd() {
	for (int k = 0; k < num_rooms; ++k) {
		for (int i = 0; i < num_rooms; ++i) {
			for (int j = 0; j < num_rooms; ++j) {
				int sum = adjacency [i][k] + adjacency[k][j];
				if (adjacency[i][k] != -1 && adjacency[k][j] != -1) {
					if (adjacency[i][j] == -1) {
						adjacency[i][j] = sum;
					}
					else if (sum < adjacency[i][j]){
						adjacency[i][j] = sum;
					}
				}
			}
		}
	}
}

void find_best_path(int location, float distance, float prob, float value, int count){
	strcpy(path[count], rooms[location].name);
	rooms[location].visited = true;
	value = value + rooms[location].probability*distance;
	prob = prob - rooms[location].probability;
	bool end = true;
	
	for(int i = 0; i < num_rooms; i++){
		if(rooms[i].visited == false){
			end = false;
			if(value + prob * (distance + adjacency[location][i]) < best_time){
				find_best_path(i, distance + adjacency[location][i], prob, value, count+1);
			}
		}
	}
	
	if(end == true){
		if(value < best_time){
			best_time = value;
			for (int i = 0; i <= count; i++){
				strcpy(best_path[i], path[i]);
			}
		}
	}
	
	rooms[location].visited = false;
}


// main
int main(int argc, char *argv[]) {
	
	best_time = 1000000;

	FILE *commandfile = fopen(argv[1], "rb");

	// read the first line as the number of rooms
	fscanf(commandfile, "%d", &num_rooms);
	
	rooms = (struct node *)malloc(sizeof(struct node)*num_rooms);
	adjacency = malloc(num_rooms*sizeof(float*));
	path = malloc(sizeof(char*)*num_rooms);
	best_path = malloc(sizeof(char*)*num_rooms);
	

	// initialize adjacency matrix
	for (int i = 0; i < num_rooms; i++) {
		adjacency[i] = malloc(num_rooms*sizeof(float));
		path[i] = malloc(STR_LEN*sizeof(char));
		best_path[i] = malloc(STR_LEN*sizeof(char));
		for (int j = 0; j < num_rooms; j++){
			adjacency[i][j] = -1.0;
		}
	}
	
	// variables to store the data read for the arrays
	char room1[STR_LEN], room2[STR_LEN];
	float prob;
	int r1, r2;
	float secs;
	
	// parse info for the rooms
	// room probability
	for (int i = 0; i < num_rooms; i++){

		fscanf(commandfile, "%s %f", room1, &prob);
		strcpy(rooms[i].name, room1);
		rooms[i].probability = prob;
		rooms[i].index = i;
		rooms[i].visited = false;
	}

	// read line for number of connections
	int connections;
	fscanf(commandfile, "%d", &connections);

	// parse connection info 
	// room1 room 2 seconds
	for (int i = 0; i < connections; i++) {
		fscanf(commandfile, "%s %s %f", room1, room2, &secs);
		r1 = find_index(room1);
		r2 = find_index(room2);
		
		if (r1 == -1 || r2 == -1) {
			printf("rooms not found\n");
		}
		else {
			adjacency[r1][r2] = secs;
			adjacency[r2][r1] = secs;
		}
	}
	
	floyd();
	
	find_best_path(0, 0, 1, 0, 0);
	if (best_time < 0) {
		printf("IMPOSSIBLE to find Sophie: -1.00\n");
	}
	else {
		printf("%.2f\n", best_time);
	}
	for (int i = 0; i < num_rooms; i++) {
		printf("%s", best_path[i]);
	}
	printf("\n");

	return 0;
}